package main.java.es.formacion.cip.alejandro;

public class ColaSaldo {

	private int saldo = 100;
	private boolean disponible = false;
	
	public synchronized int get() {
		while (disponible == false) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		disponible = false;
		notifyAll();
		return saldo;
	}

	public synchronized void put(int valor) {
		while (disponible == true) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		saldo = valor;
		disponible = true;
		notifyAll();
	}
	
	public int returnSaldo() {
		return saldo;
	}
}
