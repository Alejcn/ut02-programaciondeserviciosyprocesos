package main.java.es.formacion.cip.alejandro;

public class IngresoDinero extends Thread {

	private ColaSaldo colaSaldo;
	private int cantidadIngreso = 10;
	private int numeroPersona;
	private boolean stopHilo = false;
	private int totalIngresado;
	
	public IngresoDinero(ColaSaldo colaSaldo, int numeroPersona) {
		this.colaSaldo = colaSaldo;
		this.numeroPersona = numeroPersona;
	}
	
	public void run() {
		
		while (!stopHilo) {
			synchronized(colaSaldo) {
				int saldo = colaSaldo.returnSaldo();
				int saldoMasCantidadIngreso = saldo + cantidadIngreso;
				
				if(saldo != 0) {
					colaSaldo.put(saldoMasCantidadIngreso);
					System.out.println("INGRESO // Persona: " + numeroPersona + ", Ingresa: " + cantidadIngreso + "�" + ", Saldo actual: " + colaSaldo.get() + "�");
					totalIngresado += cantidadIngreso;
					/*try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
				}
				
				else if(saldo == 0) {
					System.out.println("Persona: " + numeroPersona + ", He llegado a 0, no puedo ingresar m�s, saldo: " + saldo + ", Total ingresado: " + totalIngresado);
					stopHilo = true;
				}
			}
			
		}
	}
}
