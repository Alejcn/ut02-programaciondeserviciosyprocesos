package main.java.es.formacion.cip.alejandro;

public class RetiradaDinero extends Thread {

	private ColaSaldo colaSaldo;
	private int numerosRetiro[] = {5,20};
	private int numeroPersona;
	private boolean stopHilo = false;
	private int totalRetirado;
	
	public RetiradaDinero(ColaSaldo colaSaldo, int numeroPersona) {
		this.colaSaldo = colaSaldo;
		this.numeroPersona = numeroPersona;
	}
	
	public void run() {
		
		while (!stopHilo) {
			synchronized(colaSaldo) {
				int numeroAleatorio = (int) (Math.random()*numerosRetiro.length+0);
				int saldo = colaSaldo.returnSaldo();
				int saldoMenosNumeroAleatorio = saldo - numerosRetiro[numeroAleatorio];
				
				if(saldoMenosNumeroAleatorio >= 0){
					colaSaldo.put(saldoMenosNumeroAleatorio);
					System.out.println("RETIRO // Persona: " + numeroPersona + ", Retira: " + numerosRetiro[numeroAleatorio] + "�" + ", Saldo actual: " + colaSaldo.get() + "�");
					totalRetirado += numerosRetiro[numeroAleatorio];
					/*try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
				}
				
				
				else if(saldo == 0) {
					System.out.println("Persona: " + numeroPersona + ", He llegado a 0, no puedo sacar m�s, Saldo final: " + saldo + ", Total retirado: " + totalRetirado);
					stopHilo = true;
				}
			}
			
		}
	}
}
