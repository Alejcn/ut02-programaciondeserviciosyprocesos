package main.java.es.formacion.cip.alejandro;

public class Cajero {

	public static void main(String[] args) {
		
		ColaSaldo cola = new ColaSaldo();
		IngresoDinero persona1 = new IngresoDinero(cola, 1);
		RetiradaDinero persona2 = new RetiradaDinero(cola, 2);
		RetiradaDinero persona3 = new RetiradaDinero(cola, 3);
		persona1.start();
		persona2.start();
		persona3.start();
	}
}
